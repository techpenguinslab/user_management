import React from 'react';
import {Panel, form,Input,Button} from 'react-bootstrap';
import {Link} from 'react-router';
import request from 'superagent';

var PostWishForm = React.createClass({
	getInitialState: function() {
		return {
			titleValue: '',
			descriptionValue: '',
			fullName: ''
		};
	},
	render: function(){
		return (
						<div className="row">
							<div style={{margin:'10px',}} className= ".col-md-10 .col-md-offset-12 ">
								<form>
									<Input type="text"  placeholder="Title" onChange={this.updateInputValueTitle} />
									<Input type="textarea"  placeholder="Write Description"  rows="3" onChange={this.updateInputValueDescription} />
									<hr/>
									<p style={{marginLeft:'20px', cursor:'pointer'}} className="fa fa-camera fa-2x"></p>
									<Button style={{marginLeft:'18px',marginTop:'-13px'}} bsStyle="primary" onClick={this.loginHandler}>Post</Button>
								</form>
							</div>
						</div>
		);
	},
	updateInputValueTitle: function(evt) {
		this.setState({
			titleValue: evt.target.value
		});
	},
	updateInputValueDescription: function(evt) {
		this.setState({
			descriptionValue: evt.target.value
		});
	},
	loginHandler : function () {
		//alert("hi");
		var self = this;
		var userId = localStorage.getItem('userId');
		var access_token = localStorage.getItem('token');
		// alert("userId: "+ userId);
		// alert("access_token: "+ access_token);
		request
			.post('http://dev.yourewa.com/api/v1/users/'+userId+'/wishes?access_token='+access_token)
			.send({"title": self.state.titleValue,
				"description":self.state.descriptionValue,
				"likeCount":"0",
				"commentCount":"0",
			})
			.set('X-API-Key', 'foobar')
			.set('Accept', 'application/json')
			.end(function(err, res){
				if (err || !res.ok) {
					alert('Oh no! error');
				} else {
					// alert('Posted successfully.  ' + JSON.stringify(res.body));
				}
			});
	}


});

export default PostWishForm;