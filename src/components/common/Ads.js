import React from 'react';
import {Panel } from 'react-bootstrap';
import request from 'superagent';

var Ads = React.createClass({
    getInitialState: function(){
        return {
        };
    },
   
	render: function() {
		return (
			<div className="row">
				<div className=".col-md-4 .col-md-offset-4">
					<Panel>
                        <div className="row">
                            <div className="col-lg-12">
                             <h5>Ads</h5>
                             <div class="ui medium rectangle test ad" data-text="Medium Rectangle">
                             <img class="medium ui image" src={require('./logo.png')} width="50"></img>
                                 <h1>Title</h1>
                            <h3>Description</h3>
                            </div>
                            </div>
                        </div>
                    </Panel>
				</div>
            </div>
                );


}
});

export default Ads;