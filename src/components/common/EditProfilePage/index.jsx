import React from 'react';
import AsyncElement from '../AsyncElement';

var PreEditProfilePage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./EditProfilePage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreEditProfilePage;