import React from 'react';
import AsyncElement from '../AsyncElement';

var PreNewFeedPage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./NewFeedPage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreNewFeedPage;