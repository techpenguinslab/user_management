import React from 'react';
import {Panel, form,Input,Button,Modal} from 'react-bootstrap';
import {Link} from 'react-router';
import request from 'superagent';
import PostWishForm from './PostWishForm';

const MySmallModal = React.createClass({
	render() {
		return (
			<Modal {...this.props} bsSize="large" aria-labelledby="contained-modal-title-lg">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-lg">Create your wish</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<PostWishForm></PostWishForm>
				</Modal.Body>
			</Modal>
		);
	}
});

var PostYourWish = React.createClass({

	getInitialState: function() {
		return {
			fullName: '',
			smShow: false
		};
	},

	render: function(){
		let smClose = () => this.setState({ smShow: false });

		return (
			<div style={{marginTop:'10px',}} className="row">
				<div className=".col-md-6 ">
					<Panel>
						<div className="row">
							<div className= "col-md-6 ">
								<form>
									<h4 style={{fontFamily: 'Lato', color:'#02BEE4',}}>Hi,{"  " + this.state.fullName}</h4>
									<h4>What are you wishing today?</h4>
									<Input type="text"  placeholder="Post your wish" onClick={this.popupopenwish} />
								</form>
							</div>
						</div>
					</Panel>
				</div>

				<MySmallModal show={this.state.smShow} onHide={smClose} />

			</div>
		);
	},

	
	  popupopenwish: function () {
		 // alert("hi");
		 this.setState({ smShow: true });
	 }

});

export default PostYourWish;