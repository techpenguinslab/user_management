import React from 'react';
import AsyncElement from '../AsyncElement';

var PreSignup = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./SignupPage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreSignup;