import React, { PropTypes, Component } from 'react';
import {PageHeader} from 'react-bootstrap';

var ExplorePage = React.createClass({

  render: function() {
    return (

    	<div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-12">
              <PageHeader>Explore</PageHeader>
            </div>
          </div>
        </div>
    	</div>
      
    );
  }

});

export default ExplorePage;