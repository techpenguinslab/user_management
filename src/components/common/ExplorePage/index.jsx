import React from 'react';
import AsyncElement from '../AsyncElement';

var PreExplorePage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./ExplorePage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreExplorePage;