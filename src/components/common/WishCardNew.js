import React from 'react';
import {Panel,Nav,Navbar,NavDropdown,NavItem,MenuItem,Modal,Input } from 'react-bootstrap';
import request from 'superagent';
import TimeAgo from 'react-timeago';
import LikeCard from './LikeCard';
import CommentCard from './CommentCard';
require('velocity-animate');
require('velocity-animate/velocity.ui');

const MySmallModal = React.createClass({
    render() {
        return (
            <Modal {...this.props} bsSize="medium" aria-labelledby="contained-modal-title-mg">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-mg">Like</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <LikeCard></LikeCard>
                </Modal.Body>
            </Modal>
        );
    }
});


var WishCardNew = React.createClass({
    getInitialState: function(){
            return {
            WishTitle: '',
            WishDescription:'',
            fullName:'',
            likeCount:0,
            commentCount:'0',
            likeStatus:false,
                smShow: false
        };
     },
    
    

    handleSelect(eventKey) {
        event.preventDefault();
        alert(`selected ${eventKey}`);
    },

    componentWillMount() {
        // var access_token = localStorage.getItem('token');
        // // request.get('http://dev.yourewa.com/api/v1/wishes/getWishes?access_token='+access_token)
        //  request.get('http://dev.yourewa.com/api/v1/wishes/wishLists?wishType=explore&access_token='+access_token)
        //     .end((error, response) => {
        //             if (!error && response) {
        //                 this.setState({ WishTitle: response.body.wishes[0].wish.title });
        //                 this.setState({ WishDescription: response.body.wishes[0].wish.description });
        //                 this.setState({ fullName: response.body.wishes[0].userInfo.user.fullName });
        //                 this.setState({ created: response.body.wishes[0].wish.created });
        //                 this.setState({ likeCount: parseInt(response.body.wishes[0].wish.likeCount , 10) });
        //                 this.setState({ commentCount: response.body.wishes[0].wish.commentCount });
        //
        //                 console.log(response.body);
        //                // alert(response.body.wishes[1].wish.userId);
        //             } else {
        //                 console.log('There was an error fetching from GitHub', error);
        //             }
        //         }
        //     );
    },
    
    
        render: function(){
            let smClose = () => this.setState({ smShow: false });

            return (

            <div className="row">
                <div className=".col-md-6 .col-md-offset-3">
                    <Panel>
                        <div>
                            <div className="row">
                                <div className="col-lg-12">
                                    <img className="img-circle  " src={require('./avi.jpg')} width="50"></img>
                                    <span style={{fontFamily: '-webkit-pictograph', color:'#02BEE4', marginLeft:'6px'}}> {"  " + this.props.fullName}</span>
                                    <TimeAgo style={{fontFamily: '-webkit-body', color:'gray', marginLeft:'65px'}} date={this.props.date}/>
                                  

                                    <h3 style={{fontFamily: 'Comfortaa' ,marginTop:'40px' ,'word-break': 'break-all;' }}><b>{this.props.WishTitle}</b></h3>
                                    <h4 style={{'word-break': 'break-all;'}}>{this.props.WishDescription}</h4>
                                    <hr></hr>
                                    <div className="row">
                                    <div className="col-md-4"><a onClick={this.handlelikeclick} style={{cursor:'pointer'}} >
                                    <i className={this.props.likeStatus ? "fa fa-heart" : "fa fa-heart-o"} style={{color:"red"}} ></i> </a> 
                                    <span onClick={this.popuplike} style={{cursor:'pointer'}} > {this.props.likeCount} Like</span>
                                    </div>
                                    <div className="col-md-4"><i  className="fa fa-comments-o"style={{color: '#33BE07', marginLeft: '-169px'}} ></i>  {this.props.commentCount} Comments</div>
                                    <div className="col-md-4"><i className="fa fa-share"style={{color: '#FF1493', marginLeft: '-275px' ,}}></i> Share</div>
                                    <hr/>
                                     <div></div>
                                     <CommentCard></CommentCard>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Panel>
                </div>
                <MySmallModal show={this.state.smShow} onHide={smClose} />
            </div>
         );


},
    popuplike: function () {
        //alert("hi");
        this.setState({ smShow: true });

    },

    handlelikeclick : function () {
        //alert("liked");

        if(this.state.likeStatus){
            var newLikeCount = parseInt(this.state.likeCount , 10) - 1;
            this.setState({likeCount:newLikeCount});
            this.setState({likeStatus:false});
        }else {
            var newLikeCount = parseInt(this.state.likeCount, 10) + 1;
            this.setState({likeCount: newLikeCount});
            this.setState({likeStatus:true});
        }
    }


});

export default WishCardNew;