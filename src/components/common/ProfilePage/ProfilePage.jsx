import React, { PropTypes, Component } from 'react';
import {NavDropdown, MenuItem, DropdownButton, Input, Nav, NavItem, Panel, PageHeader, ListGroup, ListGroupItem, Button,} from "react-bootstrap";
import Router, { Link, RouteHandler } from "react-router";
import WishCard from "../WishCard"


var ProfilePage;
ProfilePage = React.createClass({
  getInitialState: function () {
    return {}
  },


  render: function () {
    return <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8 ">
            <div className="row">
              <div className="col-lg-12">
                <PageHeader>Profile</PageHeader>
                 <Panel>
                  <div>
                   <div className="row">
                    <div style={{marginTop: '35px'}} className="col-lg-3">
                     <img className="img-circle  " src={require('../avi.jpg')} width="110"></img>
                      <h4>Abinash</h4>
                    </div>
                    <div style={{marginTop: '35px'}} className="col-lg-3">
                      <h4 style={{marginLeft:'30px'}}>0</h4>
                      <h3>Wishes</h3>
                    </div>
                    <div style={{marginTop: '35px'}} className="col-lg-3">
                      <h4 style={{marginLeft:'30px'}}>0</h4>
                      <h3>Followers</h3>
                    </div>
                    <div style={{marginTop: '35px'}} className="col-lg-3">
                      <h4 style={{marginLeft:'30px'}}>0</h4>
                      <h3>Following</h3>
                    </div>
                  </div>
                   <div className="row">
                    <Link to="EditProfilePage"><Button style={{width: '400px' ,marginLeft:'196px' ,marginTop:'-35px'}} bsStyle="primary" className="btn-outline" bsSize="small"  block>Edit Profile</Button></Link>
                   </div>
              <hr></hr>
                    <WishCard></WishCard>

            </div>
            <div className="col-lg-1"></div>
          </Panel>
          </div>
          </div>
        </div>
      </div>
     </div>
    </div>;
     }

  });

export default ProfilePage;;