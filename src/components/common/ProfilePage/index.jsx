import React from 'react';
import AsyncElement from '../AsyncElement';

var PreProfilePage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./ProfilePage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreProfilePage;