import React from 'react';
import {Panel, form,Input,Button} from 'react-bootstrap';
import Router,  { Link, RouteHandler } from 'react-router';
import request from 'superagent';

var LoginForm = React.createClass({
	getInitialState: function() {
		return {
			emailValue: '',
			passwordValue: ''
		};
	},
	mixins: [Router.Navigation],

	render: function(){
		return (
			<div className="row">
				<div className="col-md-12">
					
						<div className="row">
							<div className="col-lg-12">
								<form>
									<Input type="text" label="Email" placeholder="Email" onChange={this.updateInputValueEmail} />
									<Input type="password" label="Password" placeholder="Password"  onChange={this.updateInputValuePassword} />
									<center><Button bsStyle="primary" onClick={this.loginHandler}>Login</Button></center>
								</form>
								<center style={{color:'#0fb0b0'}}><Link to="SignupPage"><h5  style={{color:'white'}}>Create a new account,Login Here</h5></Link></center>
							</div>
						</div>
					
				</div>
			</div>
            
		);
	},
	updateInputValueEmail: function(evt) {
		this.setState({
			emailValue: evt.target.value
		});
	},
	updateInputValuePassword: function(evt) {
		this.setState({
			passwordValue: evt.target.value
		});
	},
	loginHandler : function () {
		//alert("hi");
		var self = this;
		request
			.post('http://api.yourewa.com/api/v1/users/login')
			.send({"email": self.state.emailValue,
				"password":self.state.passwordValue
			})
			.set('X-API-Key', 'foobar')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Origin', '*')
			.end(function(err, res){
				if (err || !res.ok) {
					alert('Oh no! error');
				} else {
					// alert('yay got ' + JSON.stringify(res.body));
					// alert(res.body.userId);
					localStorage.setItem('userId',res.body.userId);
					localStorage.setItem('token',res.body.id);
					var userId = localStorage.getItem('userId');
					var access_token = localStorage.getItem('token');
					self.transitionTo('NewHomePage');
					// alert("userId: "+ userId);
					// alert("access_token: "+ access_token);
				}
			});
	}


});

export default LoginForm;