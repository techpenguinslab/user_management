import React from 'react';
import AsyncElement from '../AsyncElement';

var PreChangePasswordPage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./ChangePasswordPage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreChangePasswordPage;