import React, {  Component } from 'react';
import {NavDropdown, MenuItem, DropdownButton, Input, Nav, NavItem, Panel, PageHeader, ListGroup, ListGroupItem, Button,} from "react-bootstrap";
import Router, { Link, RouteHandler } from "react-router";


var ChangePassword;
ChangePassword = React.createClass({
  getInitialState: function () {
    return {}
  },


  render: function () {
    return <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8 .col-md-offset-3">
          <PageHeader>Change Password</PageHeader>
          <Panel>
             <div>
              <div className="row">
                <div className="col-lg-9">
                  <h4>&nbsp;Old Password</h4>
                  <Input type="Password"placeholder="Old Password" />
                  <h4>&nbsp;New Password</h4>
                  <Input type="Password"placeholder="New Password" />
                  <h4>&nbsp;Re-type Password</h4>
                  <Input type="Password"placeholder="Re-type Password" />
                  <Button bsStyle="primary" className="btn-outline">Save Change</Button>

                </div>
            </div>
               </div>
          </Panel>
          </div>
        </div>
      </div>
      </div>
     }

  });

export default ChangePassword;