import React from 'react';
import {Panel, Well} from 'react-bootstrap';
import {Link} from 'react-router';

var HiComponent = React.createClass({
	render: function(){
		return (
			<div className="row">
				<div className="col-lg-12">
					<Well>
						<h4>Hi {this.props.text}</h4>
						</Well>
				</div>
			</div>
            
		);
	}
});

export default HiComponent;