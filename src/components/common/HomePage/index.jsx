import React from 'react';
import AsyncElement from '../AsyncElement';

var PreHomePage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./HomePage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreHomePage;