import React, { PropTypes, Component } from 'react';
import {NavDropdown, MenuItem, DropdownButton, Input, Nav, NavItem, Panel, PageHeader, ListGroup, ListGroupItem, Button,} from "react-bootstrap";
import Router, { Link, RouteHandler } from "react-router";
import WishCard from "../WishCard";
import PostYourWish from "../PostYourWish";
import $ from "jquery";
import request from 'superagent';


var HomePage;
HomePage = React.createClass({
  getInitialState: function(){
    return {
      WishTitle: '',
      WishDescription:'',
      fullName:'',
      likeCount:0,
      commentCount:'0',
      likeStatus:false,
      smShow: false,
      wishArray:["asd"],
      titles:["a","b"]
    };
  },
  componentWillMount() {
    this.setState({Height: $(window).height(),
      titles:["a","b"]}
    );

    var access_token = localStorage.getItem('token');
    // request.get('http://dev.yourewa.com/api/v1/wishes/getWishes?access_token='+access_token)
    request.get('http://192.168.1.8:3000/api/v1/wishes/wishLists?wishType=explore&access_token='+access_token)
        .end((error, response) => {
              if (!error && response) {

                var newarray = [];
                for(var i =0; i <12; i++){
                  newarray = response.body.wishes[0].userInfo.user.fullName;
                }
                this.setState({ wishArray:newarray });

                this.setState({ WishTitle: response.body.wishes[0].wish.title });
                this.setState({ WishDescription: response.body.wishes[0].wish.description });
                this.setState({ fullName: response.body.wishes[0].userInfo.user.fullName });
                this.setState({ created: response.body.wishes[0].wish.created });
                this.setState({ likeCount: parseInt(response.body.wishes[0].wish.likeCount , 10) });
                this.setState({ commentCount: response.body.wishes[0].wish.commentCount });

                console.log(response.body);
                // alert(response.body.wishes[1].wish.userId);
              } else {
                console.log('There was an error fetching from GitHub', error);
              }
            }
        );
  },


  render: function () {
    var self = this;
    return <div>
      <div className="container-fluid">

        <div className="row">
          <div className="col-lg-8">
            <div className="row">
              <div className="col-lg-12">
                <PostYourWish></PostYourWish>

                { this.state.titles.map(function(index) {
                  return  <WishCard fullName={self.state.fullName} date="Nov 23, 2016" WishTitle={self.state.WishTitle} WishDescription={self.state.WishDescription} likeCount={self.state.likeCount} likeStatus={self.state.likeStatus} commentCount={self.state.commentCount}></WishCard>;
                })}
              </div>
              <div className="col-lg-1"></div>
            </div>
          </div>


        </div>
      </div>
    </div>;
     }

  });

export default HomePage;