import React from 'react';
import {Panel, form,Input,Button} from 'react-bootstrap';
import {Link,Redirect} from 'react-router';
import request from 'superagent';

var Signup = React.createClass({
    getInitialState: function() {
        return {
            fullnameValue: '',
            phoneValue: '',
            emailValue: '',
            passwordValue: ''
        };
    },
    render: function(){
        return (
            <div className="row">
                <div className="col-md-12">

                        <div className="row">
                            <div className="col-lg-12">
                                <form>
                                    <Input type="text" label="Fullname" placeholder="Fullname" onChange={this.updateInputValueFullname} />
                                    <Input type="text" label="phone" placeholder="phone" onChange={this.updateInputValuePhone} />
                                    <Input type="text" label="Email" placeholder="Email" onChange={this.updateInputValueEmail} />
                                    <Input type="password" label="Password" placeholder="Password"  onChange={this.updateInputValuePassword} />
                                    <Button bsStyle="primary" onClick={this.signupHandler}>Signup</Button>
                                   
                                </form>
                                <center style={{color:'#0fb0b0'}}><Link to="NewLoginPage"><h5  style={{color:'white'}}>Already have account</h5></Link></center>
                            </div>
                        </div>
                </div>
            </div>

        );
    },
    updateInputValueFullname: function(evt) {
        this.setState({
            fullnameValue: evt.target.value
        });
    },
    updateInputValuePhone: function(evt) {
        this.setState({
            phoneValue: evt.target.value
        });
    },
    updateInputValueEmail: function(evt) {
        this.setState({
            emailValue: evt.target.value
        });
    },
    updateInputValuePassword: function(evt) {
        this.setState({
            passwordValue: evt.target.value
        });
    },
    signupHandler : function () {
        //alert("hi");
        var self = this;
        request
            .post('http://192.168.1.8:3000/api/v1/users')
            .send({"fullName": self.state.fullnameValue,
                "phone": self.state.phoneValue,
                "email": self.state.emailValue,
                "password":self.state.passwordValue
            })
            .set('X-API-Key', 'foobar')
            .set('Accept', 'application/json')
            .end(function(err, res){
                if (err || !res.ok) {
                     alert('Oh no! error');
                } else {
                    // alert('successfully sign up' + JSON.stringify(res.body));
                    
                }
            });
    }


});

export default Signup;