import React, { PropTypes, Component } from 'react';
import {PageHeader,Input,Button} from 'react-bootstrap';
import Router, { Link, RouteHandler} from "react-router";
import WishCard from "../WishCard"
import PostWishForm from "../PostWishForm";
import PostYourWish from "../PostYourWish";
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, ProgressBar, Accordion, Panel} from "react-bootstrap";
import Ads from "../Ads";
import FriendSuggestion from "../FriendSuggestion.js";
import WishCardNew from "../WishCardNew";
import $ from "jquery";
import request from 'superagent';

var NewHomePage = React.createClass({

  getInitialState: function(){
    return {
      WishTitle: '',
      WishDescription:'',
      fullName:'',
      likeCount:0,
      commentCount:'0',
      likeStatus:false,
      smShow: false,
      wishArray:["asd"]
    };
  },


  componentWillMount() {
    this.setState({Height: $(window).height(),
      titles:["a","b"]}
   );

    var access_token = localStorage.getItem('token');
    // request.get('http://dev.yourewa.com/api/v1/wishes/getWishes?access_token='+access_token)
    request.get('http://api.yourewa.com/api/v1/wishes/wishLists?wishType=explore&access_token='+access_token)
        .end((error, response) => {
              if (!error && response) {

                var newarray = [];
                for(var i =0; i <12; i++){
                  newarray = response.body.wishes[0].userInfo.user.fullName;
                }
                this.setState({ wishArray:newarray });

                this.setState({ WishTitle: response.body.wishes[0].wish.title });
                this.setState({ WishDescription: response.body.wishes[0].wish.description });
                this.setState({ fullName: response.body.wishes[0].userInfo.user.fullName });
                this.setState({ created: response.body.wishes[0].wish.created });
                this.setState({ likeCount: parseInt(response.body.wishes[0].wish.likeCount , 10) });
                this.setState({ commentCount: response.body.wishes[0].wish.commentCount });

                console.log(response.body);
                // alert(response.body.wishes[1].wish.userId);
              } else {
                console.log('There was an error fetching from GitHub', error);
              }
            }
        );
  },


  render: function() {
    var self = this;
    return (

        <div>

              <Navbar brand={<span style={{marginTop: '0px', position: 'absolute', left: 30}}><img src={require('../logo.png')} alt="Start React" title="Start React" />
                <span style={{fontFamily: 'Raleway, sans-serif'}}><Link to="HomePage"> &nbsp;ewa </Link> </span><br/>
                  <div style={{width: 300 + 'px', position: 'absolute', left: 265, top: 10 }}>
                 <Input type="text"placeholder="EWA Search"  buttonAfter= <Button><i className="fa fa-search"></i></Button> />
                 <span style={{color:'#3fc2fb',fontSize:'15px',width: 300 + 'px', position: 'absolute', left: 310, top: 8 }}>EVERY WISH ANSWERED</span>
                </div>

                </span>} fluid={true}  style={ {margin: 0} }>
                <ul className="nav navbar-top-links navbar-right">
                  <Nav style={ {margin: 0} }>

                    <NavDropdown title=<i style={{color:'#3fc2fb'}} className="fa fa-envelope fa-fw"></i> >
                    <MenuItem eventKey="1">
                      <div> <strong>Abinash</strong> <span className="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                      <div>How Are You Today??</div>

                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="2">
                      <div> <strong>Manav</strong> <span className="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                      <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="3">
                      <div> <strong>Mantavya</strong> <span className="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                      <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4">
                      <strong>Read All Messages</strong> <i className="fa fa-angle-right"></i>
                    </MenuItem>
                  </NavDropdown>

                  <NavDropdown title=<i style={{color:'#3fc2fb'}} className="fa fa-bell fa-fw"></i> >
                  <MenuItem eventKey="1" style={ {width: 300} }>
                    <div> <i className="fa fa-comment fa-fw"></i> New Comment <span className="pull-right text-muted small">4 minutes ago</span> </div>
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="2">
                    <div> <i className="fa fa-twitter fa-fw"></i> 3 New Followers <span className="pull-right text-muted small">12 minutes ago</span> </div>
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="3">
                    <div> <i className="fa fa-envelope fa-fw"></i> Message Sent <span className="pull-right text-muted small">4 minutes ago</span> </div>
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="4">
                    <div> <i className="fa fa-tasks fa-fw"></i> New Task <span className="pull-right text-muted small">4 minutes ago</span> </div>
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="5">
                    <div> <i className="fa fa-upload fa-fw"></i> Server Rebooted <span className="pull-right text-muted small">4 minutes ago</span> </div>
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="6">
                    <strong>See All Alerts</strong> <i className="fa fa-angle-right"></i>
                  </MenuItem>
                </NavDropdown>


                <NavDropdown title=<i style={{color:'#3fc2fb'}}className="fa fa-user fa-fw"></i> >
                    <MenuItem eventKey="1">
                      <i className="fa fa-user fa-fw"></i> User Profile
                    </MenuItem>
                    <MenuItem eventKey="2">
                      <i className="fa fa-gear fa-fw"></i> Settings
                    </MenuItem>                   
                    <MenuItem divider />
                    <MenuItem eventKey="4">
                      <Link to="NewLoginPage">
                        <i className="fa fa-sign-out fa-fw"></i> Logout
                      </Link>
                    </MenuItem>
                  </NavDropdown>
                </Nav>
                </ul>

          <div className="navbar-default sidebar" style={ {marginTop: '55px', 'marginLeft': '10px',width: '200px', color: '#31b9b5' } } role="navigation">
            <div className="sidebar-nav navbar-collapse">

              <ul className="nav in" id="side-menu">

                <li>
                  <Link to="ProfilePage">&nbsp;Profile</Link>
                </li>

                <li>
                  <Link to="ExplorePage"> &nbsp;Explore</Link>
                </li>

                <li>
                  <Link to="NewFeedPage"> &nbsp;New Feed</Link>
                </li>

                <li>
                  <Link to="HomePage"> &nbsp;My Wishes</Link>
                </li>

                <li>
                  <Link to="EditProfilePage"> &nbsp;Edit Profile</Link>
                </li>

              </ul>
                </div>
              </div>
              </Navbar>

              <div id="page-wrapper" className="page-wrapper" ref="pageWrapper" style={{minHeight: this.state.Height}}>
                <RouteHandler {...this.props} />             
              </div>
        </div>
    );
  }

});

export default NewHomePage;