import React from 'react';
import AsyncElement from '../AsyncElement';

var PreNewHomePage = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./NewHomePage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreNewHomePage;