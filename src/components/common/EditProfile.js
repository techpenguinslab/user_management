import React, {  Component } from 'react';
import {NavDropdown, MenuItem, DropdownButton, Input, Nav, NavItem, Panel, PageHeader, ListGroup, ListGroupItem, Button,} from "react-bootstrap";
import Router, { Link, RouteHandler } from "react-router";


var EditProfile;
EditProfile = React.createClass({
  getInitialState: function () {
    return {}
  },


  render: function () {
    return <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8 .col-md-offset-3">
          <PageHeader>Edit Profile</PageHeader>
          <Panel>
             <div>
               <h4>Change Image</h4>
              <div className="row">
                <div style={{borderLeft:'thin solid #312b2b' ,marginLeft:'180px' ,marginTop:'-35px'}} className="col-lg-9">
                  <h4>&nbsp;Full Name</h4>
                  <Input type="text"placeholder="Full Name" />
                  <h4>&nbsp;Phone Number</h4>
                  <Input type="text"placeholder="Phone Number" />
                  <h4>&nbsp;Date Of Birth</h4>
                  <Input type="text"placeholder="Date Of Birth" />
                  <h4>&nbsp;Gender</h4>
                  <Input type="select">
                    <option value="1">Male</option>
                    <option value="1">Female</option>
                  </Input>
                  <h4>&nbsp;Email</h4>
                  <Input type="text"placeholder="Email" />
                  <Button bsStyle="primary" className="btn-outline">Save Profile</Button>

                </div>
            </div>
               </div>
          </Panel>
          </div>
        </div>
      </div>
      </div>
     }

  });

export default EditProfile;