import React from 'react';
import AsyncElement from '../AsyncElement';

var PreNewLogin = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./NewLoginPage.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreNewLogin;