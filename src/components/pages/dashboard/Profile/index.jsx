import React from 'react';
import AsyncElement from '../../../common/AsyncElement';

var PreProfile = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./Profile.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreProfile;