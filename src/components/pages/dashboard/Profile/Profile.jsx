import React, { PropTypes, Component } from 'react';
import {PageHeader,Input} from 'react-bootstrap';

var Profile = React.createClass({

    render: function() {
        return (

            <div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-12">
                            <PageHeader>Profile</PageHeader>
                            <h2>Name :xyz</h2>
                            <h2>Mobile :1234567890</h2>
                            <h2>Email :xyz@gmail.com</h2>
                            <h2>Gender :male</h2>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

});

export default Profile;