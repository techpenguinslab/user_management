import React, { PropTypes, Component } from 'react';
import Router, { Link, RouteHandler } from "react-router";
import {Pagination, Panel, Well, Button, PageHeader} from "react-bootstrap";

var Tables = React.createClass({
  getInitialState: function() {
    return {
      isSelected: false
    };
  },
  handleClick: function() {
    // this.setState({
    //   isSelected: true
    // })
    window.open("hi");
  },
  render: function() {
    return (

      <div>
        <div className="col-lg-12">
          <PageHeader>Sell Wishes</PageHeader>
        </div>
       			<div>
       				<div className="dataTable_wrapper">
                <div id="dataTables-example_wrapper" className="dataTables_wrapper form-inline dt-bootstrap no-footer">

                  <div className="row">
                    <div className="col-sm-9">
                      <div className="dataTables_length" id="dataTables-example_length">
                        <label>Status <select name="dataTables-example_length" aria-controls="dataTables-example" className="form-control input-sm"><option text="open">Open</option><option text>Approved</option><option text>Rejected</option><option text>Re-review</option></select></label>
                      </div>
                    </div>
                    <div className="col-sm-3">
                      <div id="dataTables-example_filter" className="dataTables_filter">
                        <td><Button type="submit" bsSize="primary" bsStyle="info" block>Priview</Button></td>
                        <td><Button type="submit" bsSize="small" bsStyle="info" block>Next</Button></td>

                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-sm-12">
                      <table className="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                        <thead>
                          <tr role="row"><th className="sorting_asc" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="SL.No.: activate to sort column descending" aria-sort="ascending" style={ {width: 165} }>Ticket ID</th><th className="sorting" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="UserID: activate to sort column ascending" style={ {width: 321} }>Product Name</th><th className="sorting" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="Email: activate to sort column ascending" style={ {width: 199} }>Brand</th><th className="sorting_asc" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="SL.No.: activate to sort column descending" aria-sort="ascending" style={ {width: 165} }>Categorys</th><th className="sorting" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="Mobile No.: activate to sort column ascending" style={ {width: 231} }>Status</th><th className="sorting_asc" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="SL.No.: activate to sort column descending" aria-sort="ascending" style={ {width: 165} }>Created on</th><th className="sorting" tabIndex="0" aria-controls="dataTables-example" rowSpan="1" colSpan="1" aria-label="Detail: activate to sort column ascending" style={ {width: 180} }>Detail</th></tr>
                        </thead>
                        <tbody>
                          <tr className="gradeA odd" role="row">
                            <td className="sorting_1">6</td>
                            <td>Gng karbon</td>
                            <td>Karbonn</td>
                            <td>batteries</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 14:25</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                          <tr className="gradeA even" role="row">
                            <td className="sorting_1">5</td>
                            <td>Inspiron</td>
                            <td>Dell</td>
                            <td>laptops</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 14:22</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                          <tr className="gradeA odd" role="row">
                            <td className="sorting_1">4</td>
                            <td>Racold 15 litre water heater</td>
                            <td>Racold</td>
                            <td>geysers</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 16:33</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                          <tr className="gradeA even" role="row">
                            <td className="sorting_1">3</td>
                            <td>Philips TV</td>
                            <td>Philips</td>
                            <td>tv</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 12:05</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                          <tr className="gradeA odd" role="row">
                            <td className="sorting_1">2</td>
                            <td>Pendrive</td>
                            <td>SanDisk</td>
                            <td>computer-pendrive</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 10:25</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                          <tr className="gradeA even" role="row">
                            <td className="sorting_1">1</td>
                            <td>Microns mobile</td>
                            <td>Micromax</td>
                            <td>mobile</td>
                            <td>Rejected Earlier</td>
                            <td>29/8/2016 10:13</td>
                            <td><Link to="dashboard.profile"><Button type="submit" bsSize="small" bsStyle="info" block>Full Profile View</Button></Link></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>



      </div>
    );
  }

});

export default Tables;