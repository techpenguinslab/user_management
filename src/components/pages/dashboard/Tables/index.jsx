import React from 'react';
import AsyncElement from '../../../common/AsyncElement';

var PreTables = React.createClass({

  mixins: [ AsyncElement ],

  bundle: require('bundle?lazy!./Users.jsx'),

  preRender: function () {
  	return <div></div>;
  }
});

export default PreTables;